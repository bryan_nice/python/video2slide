# Video2Slides

Builds a container to extract slides from video lectures.

## Command Line Options

| Option | Default Value | Description |
| :----- | :------------ | :---------- |
| --detect_shadows | False | If true, the algorithm will detect shadows and mark them. |
| --fgbg_history | 45 | Number of frames in background object |
| --frame_rate | 3 | Number of frames per second that needs to be processed, fewer the count faster the speed. |
| --input_file |  | Input video file to extract slides from. |
| --max_percent | 3 | Max percentage of diff between foreground and background to detect if frame is still in motion. |
| --min_percent | 0.1 | Min percentage of diff between foreground and background to detect if motion has stopped. |
| --output_path | ./output | Output to store the slides pdf file. |
| --var_threshold | 16 | Threshold on the squared Mahalanobis distance between the pixel and the model to decide whether a pixel is well described by the background model. |
| --warm_up | 3 | Initial number of frames to be skipped. |

## Usage Example

```bash
docker run \
  -it \
  --rm \
  -v <LOCAL DIRECTORY>:/home/video2slides \
  video2slides \
  --input_file="/home/video2slides/VIDEO FILE NAME"
```

## Reference

+ [Gitlab repo bryan_nice/video2slides to this container image](https://gitlab.com/bryan_nice/python/video2slides)
+ [Based on GitHub Repo kaushikj/video2pdf](https://github.com/kaushikj/video2pdf)
