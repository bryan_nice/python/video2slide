#!/usr/bin/env python3

import argparse
import cv2
import glob
import img2pdf
import imutils
import os
import shutil
import time

class Parse:
    def __init__(self):
        self.command_line()

    def command_line(self):
        parser = argparse.ArgumentParser()

        parser.add_argument("--detect_shadows", default=False, dest="detectShadows", help="If true, the algorithm will detect shadows and mark them.")
        parser.add_argument("--fgbg_history", default=45, dest="fgbgHistory", help="Number of frames in background object")
        parser.add_argument("--frame_rate", default=3, dest="frameRate", help="Number of frames per second that needs to be processed, fewer the count faster the speed.")
        parser.add_argument("--input_file", dest="inputFile", help="Input video file to extract slides from.")
        parser.add_argument("--max_percent", default=3, dest="maxPercent", help="Max percentage of diff between foreground and background to detect if frame is still in motion.")
        parser.add_argument("--min_percent", default=0.1, dest="minPercent", help="Min percentage of diff between foreground and background to detect if motion has stopped.")
        parser.add_argument("--output_path", default="./output", dest="outputPath", help="Output to store the slides pdf file.")
        parser.add_argument("--var_threshold", default=16, dest="varThreshold", help="Threshold on the squared Mahalanobis distance between the pixel and the model to decide whether a pixel is well described by the background model.")
        parser.add_argument("--warm_up", default=3, dest="warmUp", help="Initial number of frames to be skipped.")

        args = parser.parse_args()

        self.detect_shadows = args.detectShadows
        self.fgbg_history = args.fgbgHistory
        self.frame_rate = args.frameRate
        self.input_file = args.inputFile
        self.max_percent = args.maxPercent
        self.min_percent = args.minPercent
        self.output_path = args.outputPath
        self.var_threshold = args.varThreshold
        self.warm_up = args.warmUp

class Video2Slides:
    def __init__(self, myConfig):
        self.detect_shadows = myConfig.detect_shadows
        self.fgbg_history = myConfig.fgbg_history
        self.frame_rate = myConfig.frame_rate
        self.input_file = myConfig.input_file
        self.max_percent = myConfig.max_percent
        self.min_percent = myConfig.min_percent
        self.output_path = myConfig.output_path
        self.var_threshold = myConfig.var_threshold
        self.warm_up = myConfig.warm_up

    def get_frames(self):
        # fucntion to return the frames from a video located at video_path
        #this function skips frames as defined in FRAME_RATE


        # open a pointer to the video file initialize the width and height of the frame
        vs = cv2.VideoCapture(self.input_file)
        if not vs.isOpened():
            raise Exception(f'unable to open file {self.input_file}')


        total_frames = vs.get(cv2.CAP_PROP_FRAME_COUNT)
        frame_time = 0
        frame_count = 0
        print("total_frames: ", total_frames)
        print("FRAME_RATE", self.frame_rate)

        # loop over the frames of the video
        while True:
            # grab a frame from the video

            vs.set(cv2.CAP_PROP_POS_MSEC, frame_time * 1000)    # move frame to a timestamp
            frame_time += 1/self.frame_rate

            (_, frame) = vs.read()
            # if the frame is None, then we have reached the end of the video file
            if frame is None:
                break

            frame_count += 1
            yield frame_count, frame_time, frame

        vs.release()

    def initialize_output_folder(self):
        # Clean the output folder if already exists
        self.output_folder_screenshot_path = os.path.join(
            self.output_path,
            self.input_file.rsplit("/")[-1].split(".")[0]
        )

        if os.path.exists(self.output_folder_screenshot_path):
            shutil.rmtree(self.output_folder_screenshot_path)

        os.makedirs(self.output_folder_screenshot_path, exist_ok=True)
        print("initialized output folder", self.output_folder_screenshot_path)

    def detect_unique_screenshots(self):
        # Initialize fgbg a Background object with Parameters
        # history = The number of frames history that effects the background subtractor
        # varThreshold = Threshold on the squared Mahalanobis distance between the pixel and the model to decide whether a pixel is well described by the background model. This parameter does not affect the background update.
        # detectShadows = If true, the algorithm will detect shadows and mark them. It decreases the speed a bit, so if you do not need this feature, set the parameter to false.

        fgbg = cv2.createBackgroundSubtractorMOG2(
            history=self.fgbg_history,
            varThreshold=self.var_threshold,
            detectShadows=self.detect_shadows
        )

        captured = False
        start_time = time.time()
        (W, H) = (None, None)

        screenshoots_count = 0
        for frame_count, frame_time, frame in self.get_frames():
            orig = frame.copy() # clone the original frame (so we can save it later),
            frame = imutils.resize(frame, width=600) # resize the frame
            mask = fgbg.apply(frame) # apply the background subtractor

            # apply a series of erosions and dilations to eliminate noise
            # eroded_mask = cv2.erode(mask, None, iterations=2)
            # mask = cv2.dilate(mask, None, iterations=2)

            # if the width and height are empty, grab the spatial dimensions
            if W is None or H is None:
                (H, W) = mask.shape[:2]

            # compute the percentage of the mask that is "foreground"
            p_diff = (cv2.countNonZero(mask) / float(W * H)) * 100

            # if p_diff less than N% then motion has stopped, thus capture the frame

            if p_diff < self.min_percent and not captured and frame_count > self.warm_up:
                captured = True
                filename = f"{screenshoots_count:03}_{round(frame_time/60, 2)}.png"

                path = os.path.join(
                    self.output_folder_screenshot_path,
                    filename
                )
                print("saving {}".format(path))
                cv2.imwrite(path, orig)
                screenshoots_count += 1

            # otherwise, either the scene is changing or we're still in warmup
            # mode so let's wait until the scene has settled or we're finished
            # building the background model
            elif captured and p_diff >= self.max_percent:
                captured = False
        print(f'{screenshoots_count} screenshots Captured!')
        print(f'Time taken {time.time()-start_time}s')
        return

    def convert_screenshots_to_pdf(self):

        print(self.input_file.rsplit("/")[-1].split(".")[0])
        output_pdf_path = os.path.join(
            self.output_path,
            self.input_file.rsplit("/")[-1].split(".")[0] + "_slides.pdf"
        )

        print("output_folder_screenshot_path", self.output_folder_screenshot_path)
        print("output_pdf_path", output_pdf_path)
        print("converting images to pdf..")
        with open(output_pdf_path, "wb") as f:
            f.write(
                img2pdf.convert(
                    sorted(
                        glob.glob(
                            os.path.join(
                                self.output_folder_screenshot_path,
                                "*.png"
                            )
                        )
                    )
                )
            )
        print("Pdf Created!")
        print("pdf saved at", output_pdf_path)

    def clean_up(self):
        shutil.rmtree(self.output_folder_screenshot_path)

if __name__ == "__main__":

    config = Parse()
    print("video_path", config.input_file)

    video2Slides = Video2Slides(config)
    video2Slides.initialize_output_folder()
    video2Slides.detect_unique_screenshots()

    print("Generating pdf")
    video2Slides.convert_screenshots_to_pdf()
    video2Slides.clean_up()
