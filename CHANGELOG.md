# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
[markdownlint](https://dlaa.me/markdownlint/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2023-01-07

### Added to 0.1.0

- Added clean up logic
- removed prompt to create pdf

## [0.0.0] - 2023-01-03

### Added to 0.0.0

- Initial release
