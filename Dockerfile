FROM python:3.11-slim-bullseye

RUN apt-get update &&  \
    apt-get install -y \
      libgl1  \
      libglib2.0-0 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt && \
    rm -rf requirements.txt

COPY video2slides.py /usr/bin/video2slides.py

RUN chmod +x /usr/bin/video2slides.py

RUN adduser video2slides --home /home/video2slides --disabled-password --quiet

USER video2slides

WORKDIR /home/video2slides

ENTRYPOINT ["video2slides.py"]
